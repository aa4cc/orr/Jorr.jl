using Jorr
using LinearAlgebra
using Test

@testset "Approximate line search" begin
    @test begin
        f(x) = x[1]^2+x[1]*x[2]+x[2]^2
        ∇f(x) = [2x[1]+x[2],2x[2]+x[1]]
        x₀ = [1.0,2.0]
        ∇f₀ = ∇f(x₀)
        d = [-1.0,-1.0]
        α = backtracking_line_search(f,∇f₀,x₀,d,α₀=10.0)
        α≈2.5
    end
end

@testset "Gradient descent minimization" begin
    @test begin
        f(x) = (x[1]+2x[2]-7)^2 + (2x[1]+x[2]-5)^2
        x₀ = [1.0,1.0]
        x = gradient_descent_minimization(f,x₀)
        norm(x-[1.0,3.0])<=1e-8
    end
end

@testset "Newton minimization" begin
    @test begin
        f(x) = (x[1]+2x[2]-7)^2 + (2x[1]+x[2]-5)^2
        x₀ = [1.0,1.0]
        x = newton_minimization(f,x₀)
        norm(x-[1.0,3.0])<=1e-8
    end
end

@testset "Quasi-Newton minimization" begin
    @test begin
        f(x) = (x[1]+2x[2]-7)^2 + (2x[1]+x[2]-5)^2
        x₀ = [1.0,1.0]
        x = quasi_newton_minimization(f,x₀)
        norm(x-[1.0,3.0])<=1e-8
    end
end
