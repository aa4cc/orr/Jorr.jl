using Jorr
using Documenter

DocMeta.setdocmeta!(Jorr, :DocTestSetup, :(using Jorr); recursive=true)

makedocs(;
    modules=[Jorr],
    authors="Zdeněk Hurák <hurak@fel.cvut.cz>",
    repo="https://gitlab.fel.cvut.cz/hurak/Jorr.jl/blob/{commit}{path}#{line}",
    sitename="Jorr.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://hurak.gitlab.io/Jorr.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
