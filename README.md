# Jorr.jl

[![pipeline status](https://gitlab.fel.cvut.cz/aa4cc/orr/Jorr.jl/badges/master/pipeline.svg)](https://gitlab.fel.cvut.cz/aa4cc/orr/Jorr.jl/-/commits/master)
[![coverage report](https://gitlab.fel.cvut.cz/aa4cc/orr/Jorr.jl/badges/master/coverage.svg)](https://gitlab.fel.cvut.cz/aa4cc/orr/Jorr.jl/-/commits/master)

Julia package implementing (some) algorithms presented/studied in the [Optimal and Robust Control (ORR)](moodle.fel.cvut.cz/course/B3M35ORR) course. For explanation-rich Jupyter notebooks see the [Examples](https://gitlab.fel.cvut.cz/aa4cc/orr/examples) repository here at Gitlab.

The algorithms are not particularly optimized for performance. Their main purpose is to help understand the theory through playing with the code and demonstration of its functionality. But any suggestion on improving the performance without reducing the readability of the code is welcome.
