module Jorr

using BlockArrays
using ControlSystems
using Convex
using DifferentialEquations
using FiniteDiff
using Interpolations
using LinearAlgebra
using NLsolve
using Plots
using Printf
using Roots
using SCS
using SparseArrays
using Zygote

include("optimization.jl")

export backtracking_line_search
export gradient_descent_minimization
export newton_minimization
export quasi_newton_minimization

include("direct_discrete_time_LQ_optimal_control.jl")

export direct_dlqr_simultaneous
export direct_dlqr_sequential

include("indirect_discrete_time_LQ_optimal_control.jl")

include("dynamic_programming.jl")

export design_dp_lookup_tables
export dp_sim
export demo_first_order_dp

include("indirect_continuous_time_LQ_optimal_control.jl")

export indirect_clqr_fixed
export indirect_clqr_free
export cdre
export indirect_cocp_fixed

include("indirect_continuous_time_numerical_optimal_control.jl")

export shooting_pendulum
export simulate_cannon
export shooting_cannon
export shooting_lq_fixed

end
