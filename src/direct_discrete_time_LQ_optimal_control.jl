"""
    u,x = direct_dlqr_simultaneous(A,B,x₀,Q,R,S,N)

Compute an optimal sequence `u` of control inputs to a discrete-time linear system that minimizes the LQ cost on a finite horizon directly by formulating and solving a quadratic program (QP).

# Example

```julia
n = 2
m = 1
A = rand(n,n)
B = rand(n,m)
x0 = [1.0, 3.0]

N = 10

s = [1.0, 2.0]
q = [1.0, 2.0]
r = [1.0]

S = diagm(0=>s)
Q = diagm(0=>q)
R = diagm(0=>r)
```
"""
function direct_dlqr_simultaneous(A,B,x₀,Q,R,S,N)
    Qbar = BlockArray(spzeros(N*n,N*n),repeat([n],N),repeat([n],N))
    for i=1:(N-1)
        Qbar[Block(i,i)] = Q
    end
    Qbar[Block(N,N)] = S
    Rbar = BlockArray(spzeros(N*m,N*m),repeat([m],N),repeat([m],N))
    for i=1:N
        Rbar[Block(i,i)] = R
    end
    Qtilde = blockdiag(sparse(Qbar),sparse(Rbar))
    Bbar = BlockArray(spzeros(N*n,N*m),repeat([n],N),repeat([m],N))
    for i=1:N
        Bbar[Block(i,i)] = B
    end
    Abar = BlockArray(sparse(-1.0*I,n*N,n*N),repeat([n],N),repeat([n],N))
    for i=2:N
        Abar[Block(i,(i-1))] = A
    end
    Atilde = sparse([Abar Bbar])
    A0bar = spzeros(n*N,n)
    A0bar[1:n,1:n] = A
    btilde = A0bar*sparse(x0)

    xtilde = Variable((n+m)*N)
    problem = minimize(1/2*quadform(xtilde,Qtilde),[Atilde*xtilde + btilde == 0])
    solve!(problem, () -> SCS.Optimizer(verbose=false))
    xopt = reshape(xtilde.value[1:(n*N)],(2,:))
    uopt = reshape(xtilde.value[(n*N+1):end],(m,:))
    return xopt,uopt
end

"""
    u,x = direct_dlqr_sequential(A,B,x₀,Q,R,S,N)

Compute an optimal sequence `u` of control inputs to a discrete-time linear system that minimizes the LQ cost on a finite horizon directly by formulating and solving a quadratic program (QP).

# Example

```julia
n = 2
m = 1
A = rand(n,n)
B = rand(n,m)
x0 = [1, 3]

N = 10

s = [1, 2]
q = [1, 2]
r = [1]

S = diagm(0=>s)
Q = diagm(0=>q)
R = diagm(0=>r)

uopts,xopts = direct_dlqr_sequential(A,B,x₀,Q,R,S,N)

using Plots
p1 = plot(0:(N-1),uopts,marker=:diamond,label="u",linetype=:steppost)
xlabel!("k")
ylabel!("u")

p2 = plot(0:N,hcat(x0,xopts)',marker=:diamond,label=["x1" "x2"],linetype=:steppost)
xlabel!("k")
ylabel!("x")

plot(p1,p2,layout=(2,1))
```
"""
function direct_dlqr_sequential(A,B,x₀,Q,R,S,N)
    Qbar = BlockArray(spzeros(N*n,N*n),repeat([n],N),repeat([n],N))
    for i=1:(N-1)
        Qbar[Block(i,i)] = Q
    end
    Qbar[Block(N,N)] = S
    Rbar = BlockArray(spzeros(N*m,N*m),repeat([m],N),repeat([m],N))
    for i=1:N
        Rbar[Block(i,i)] = R
    end
    Chat = BlockArray(spzeros(N*n,N*m),repeat([n],N),repeat([m],N))
    Ahat = BlockArray(spzeros(N*n,n),repeat([n],N),[n])
    for i=1:N
        for j = 1:i
            Chat[Block(i,j)] = A^(i-j)*B
            Ahat[Block(i,1)] = A^i
        end
    end
    H = Chat'*Qbar*Chat + Rbar
    H = Array(H)
    F = Chat'*Qbar*Ahat
    F = Array(F)

    uopts = -H\(F*x0)
    xopts = Chat*uopts + Ahat*x0
    xopts = reshape(xopts,(2,:))
    return uopts,xopts
end

"""
    u,x = direct_dlqr_sequential(A,B,x₀,Q,R,S,N,(umin,umax))

Compute an optimal sequence `u` of control inputs to a discrete-time linear system that minimizes the LQ cost on a finite horizon directly by formulating and solving a quadratic program (QP).

# Example

```julia
n = 2
m = 1
A = rand(n,n)
B = rand(n,m)
x0 = [1, 3]

N = 10

s = [1, 2]
q = [1, 2]
r = [1]

S = diagm(0=>s)
Q = diagm(0=>q)
R = diagm(0=>r)
```
"""
function direct_dlqr_sequential(A,B,x₀,Q,R,S,N,(umin,umax))
    Chat = BlockArray(spzeros(N*n,N*m),repeat([n],N),repeat([m],N))
    Ahat = BlockArray(spzeros(N*n,n),repeat([n],N),[n])
    for i=1:N
        for j = 1:i
            Chat[Block(i,j)] = A^(i-j)*B
            Ahat[Block(i,1)] = A^i
        end
    end
    H = Chat'*Qbar*Chat + Rbar
    H = Array(H)
    F = Chat'*Qbar*Ahat
    F = Array(F)

    u = Variable(N*m)
    problem = minimize(1/2*quadform(u,H) + dot(F*x0,u))
    problem.constraints += u >= umin
    problem.constraints += u <= umax
    solve!(problem, () -> SCS.Optimizer(verbose=false))

    xopts = Chat*u.value + Ahat*x0
    xopts = reshape(xopts,(2,:))
    return uopts,xopts
end
