"""
    θinitial, dsol = shooting_pendulum(l,θfinal,tspan)

For an ideal pendulum desribed by the second-order model `̈θ + g/l sin(θ) = 0`, and for a choosen time span `tspan = (tinitial,tfinal)` and the value `θfinal` of the angle at the final time, compute the value of the initial angle `θinitial` while assuming the initial velocity `̇θ` is zero. Return also the solution to the ODE for the given initial state.

The problem is formulated and solved as a two-point boundary value problem (BVP) using the method of shooting.

# Example:
```julia
l = 1.0
θfinal = -0.1
tspan = (0.0,3.5)

θinitial,dsol = shooting_pendulum(l,θfinal,tspan)

p1 = plot(dsol,xlabel="Time",ylabel="Angle",label="θ",vars=(0,1))
p2 = plot(dsol,xlabel="Time",ylabel="Angular rate",label="ω",vars=(0,2))
plot(p1,p2,layout=(2,1))
```
"""
function shooting_pendulum(l,θfinal,tspan)
    g = 9.81
    function statecostateeq!(dx,x,p,t)
        θ = x[1]
        ω = x[2]
        dx[1] = ω
        dx[2] = -g/l*sin(θ)
    end
    ωinitial = 0.0
    function F(θinitial)
        xinitial = [θinitial,ωinitial]
        prob = ODEProblem(statecostateeq!,xinitial,tspan)
        dsol = solve(prob,Tsit5())
        θfinalsolved = dsol[end][1]
        return θfinal-θfinalsolved
    end
    θinitial = find_zero(F,(-pi,pi),verbose=false)       # In general can find more solutions. [TBD]
    xinitial = [θinitial,ωinitial]
    prob = ODEProblem(statecostateeq!,xinitial,tspan)   # Already solved in F(), should be reused.
    dsol = solve(prob,Tsit5())                          # But I did not care.
    return θinitial,dsol
end

"""
    simulate_cannon(vinitial,tspan)

Simulate the trajectory of a projectile shot from a cannnon at a given initial velocity.

The start of the trajectory (location of the cannon muzzle) is at `[0,0]`. The initial velocity vector `vinitial` is the parameter for the simulation (other parameters such as coefficient of the aerodynamic drag are defined inside the simulator).

Another parameter is the lenght of the simulation (through the time span parameter `tspan`). Note however, that simulation is terminated as soon as the vertical coordinate of the projectile reaches 0 again.

The function returns the full simulation outputs.

# Example:
```julia
θinitial = pi/5;
vabsinitial = 30.0;
vinitial = [vabsinitial*cos(θinitial),vabsinitial*sin(θinitial)];
tfinal = 10.0;
tspan = (0.0,tfinal);
dsol = simulate_cannon(vinitial,tspan)
plot(dsol,vars=(1,2),lw=2,legend=false,xlabel="x",ylabel="y")
```
"""
function simulate_cannon(vinitial,tspan)
    g = 9.81                                    # Gravitation acceleration.
    c = 0.2                                     # Aerodynamic drag coefficient.
    pinitial = [0.0, 0.0]                       # Initial position.
    function statecostateeq!(dw,w,p,t)
        x = w[1]
        y = w[2]
        vx = w[3]
        vy = w[4]
        v = sqrt(vx^2+vy^2)
        dw[1] = vx
        dw[2] = vy
        dw[3] = -c*vx*v
        dw[4] = -c*vy*v - g
    end
    winitial = vcat(pinitial,vinitial)
    prob = ODEProblem(statecostateeq!,winitial,tspan)
    integrator = init(prob,Tsit5())
    condition(u,t,integrator) = u[2]-pfinal[2]  # How combine with vy<=0?
    affect!(integrator) = terminate!(integrator)
    cb = ContinuousCallback(condition,affect!)
    dsol = solve(prob,callback=cb)
    return dsol
end


"""
    shooting_cannon(pfinal,tspan)

Find the vector of initial velocity (aka muzzle velocity) of a cannon projectile for a given target position.

# Example:
```julia
tfinal = 10.0
tspan=(0.0,tfinal)
pfinal = [10.0,0.0]
vinitial = shooting_cannon(pfinal,tspan)
```
"""
function shooting_cannon(pfinal,tspan)
    thetainitial = pi/4                         # Initial guess
    vabsinitial = 10.0
    vinitial = [vabsinitial*cos(thetainitial),vabsinitial*sin(thetainitial)]
    function fun!(F,vinitial)
        dsol = simulate_cannon(vinitial,tspan)
        pfinalsolved = dsol[end][1:2]           # Final position from solving the ODE.
        F = pfinal-pfinalsolved                 # Error in final position.
    end
    nsol = nlsolve(fun!,vinitial)               # Optionally: ,autodiff=:forward
    return nsol.zero
end



"""
    shooting_lq_fixed(A,B,Q,R,xinitial,xfinal,tspan)

Solve the two-point boundary value problem corresponding to the continuous-time LQ-optimal control problem on a finite time interval and with wixed final state using the method of shooting.

# Example:
```julia

```
"""
function shooting_lq_fixed(A,B,Q,R,xinitial,xfinal,tspan)
    tinitial = tspan[1]
    tfinal = tspan[2]
    function statecostateeq!(dw,w,p,t)
        x = w[1]
        λ = w[2]
        dw[1] = A*x - B\R*B*λ
        dw[2] = -Q*x - A'*λ
    end
    λinitial = 0.0
    function fun!(eq,λinitial)
        winitial = [xinitial,λinitial]
        prob = ODEProblem(statecostateeq!,winitial,tspan)
        dsol = solve(prob,Tsit5())
        xfinalsolved = dsol[end][1]
        eq[1] = xfinal-xfinalsolved
    end
    nsol = nlsolve(fun!,[λinitial],autodiff=:forward)
    return nsol.zero
end
