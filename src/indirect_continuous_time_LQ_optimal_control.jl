"""
    indirect_clqr_fixed(A,B,Q,R,x₀,x₁,tspan)

Solve the two-point boundary value problem corresponding to the continuous-time LQ-optimal control on a finite time interval with the final state fixed.

# Example
```julia
n = 2                       # Order of the system
A = rand(n,n)               # Matrices modeling the system
B = rand(n,1)

Q = diagm(0=>rand(n))       # Weighting matrices for the quadratic cost function
R = rand(1,1)

x0 = [1.0,2.0]              # Initial states
x1 = [0.0,3.0]              # Final (desired) states
t0 = 0.0                    # Initial tima
t1 = 10.0                   # Final time
tspan = (t0,t1)             # Time span

t,x,λ,u = indirect_clqr_fixed(A,B,Q,R,x₀,x₁,tspan)

using Plots
gr(xlabel="t",legend=false)

p1 = plot(t,x,ylabel="States")
p2 = plot(t,λ,ylabel="Costates")
p3 = plot(t,u,ylabel="Control")

plot(p1,p2,p3,layout=(3,1))
```
"""
function indirect_clqr_fixed(A,B,Q,R,x₀,x₁,tspan)
    t0 = tspan[1]
    t1 = tspan[2]
    H = [A B/R*B'; Q -A']   # Combined matrix for the Hamiltonian cannonical equations
    P = exp(H*t1)           # Transition matrix for the Hamiltonian system
    P11 = P[1:n,1:n]
    P12 = P[1:n,n+1:end]
    P21 = P[n+1:end,1:n]
    P22 = P[n+1:end,n+1:end]
    λ0 = P12\(x1-P11*x0);   # solving for the initial value of costate
    # Solving for the states and costates during the time interval
    G = ss(H,zeros(2n,1),Matrix{Float64}(I, 2n, 2n),0)
    w0 = [x0; λ0]
    t = range(t0,t1,length=100)
    v(w,t) = [0]
    y, t, w, uout = lsim(G,v,t,x0=w0)
    x = w[:,1:n]            # State trajectory (solution)
    λ = w[:,n+1:end]        # Costate
    u = (R\B'*λ')'          # Optimal control from the stationarity equation
    return t,x,λ,u
end

"""
    indirect_clqr_free(A,B,Q,R,S,x₀,tspan)

Solve the two-point boundary value problem corresponding to the continuous-time LQ-optimal control on a finite time interval with the final state free.

# Example
```julia
n = 2                       # Order of the system
A = rand(n,n)               # Matrices modeling the system
B = rand(n,1)

Q = diagm(0=>rand(n))       # Weighting matrices for the quadratic cost function
R = rand(1,1)
S = diagm(0=>10*ones(n))

x0 = [1.0,2.0]              # Initial states
t0 = 0.0                    # Initial time
t1 = 10.0                   # Final time
tspan = (t0,t1)             # Time span

t,x,λ,u = indirect_clqr_free(A,B,Q,R,S,x₀,tspan)

using Plots
gr(xlabel="t",legend=false)

p1 = plot(t,x,ylabel="States")
p2 = plot(t,λ,ylabel="Costates")
p3 = plot(t,u,ylabel="Control")

plot(p1,p2,p3,layout=(3,1))
```
"""
function indirect_clqr_free(A,B,Q,R,S,x₀,tspan)
    t0 = tspan[1]
    t1 = tspan[2]
    H = [A B/R*B'; Q -A']   # Combined matrix for the Hamiltonian cannonical equations
    P = exp(H*t1)           # Transition matrix for the Hamiltonian system
    P11 = P[1:n,1:n]
    P12 = P[1:n,n+1:end]
    P21 = P[n+1:end,1:n]
    P22 = P[n+1:end,n+1:end]
    λ0 = -(S*P12+P22)\(S*P11+P21)*x0  # solving for the initial value of costate
    # Solving for the states and costates during the time interval
    G = ss(H,zeros(2n,1),Matrix{Float64}(I, 2n, 2n),0)
    w0 = [x0; λ0]
    t = range(t0,t1,length=100)
    v(w,t) = [0]
    y, t, w, uout = lsim(G,v,t,x0=w0)
    x = w[:,1:n]            # State trajectory/solution
    λ = w[:,n+1:end]        # Costate
    u = (R\B'*λ')'          # Optimal control from the stationarity equation
    return t,x,λ,u
end

"""
    cdre(a,b,q,r,s₁,tspan)

Solve a scalar differential Riccati equation (CDRE) ``-d/dt s(t) = 2as(t) + q - b^2/r s^2(t)`` related to the continuous-time LQ-optimal control (regulation).

Consider a fixed time interval (t₀,t₁), on which a continuous-time LTI system described by ``\dot x = A x + B u`` should be regulated such that the cost function ``1/2 s x^2(t_1) + 1/2 \int_0^{t_1} (qx^2(t) + r u^2(t) dt)`` is minimized for any initial state ``x(0)``. The optimal control is given by solving the differential Riccati equation ``-d/dt s(t) = 2as(t) + q - b^2/r s^2(t)``, for which the value is fixed to `s₁` at the final time.

# Example
```julia
a = 1.0; b = 1.0; q = 1.0; r = 10.0; s₁ = 2.0;
t₀ = 0.0
t₁ = 30.0;

t,s = cdre(a,b,q,r,s₁,(t₀,t₁))

using Plots

plot(t,s,linewidth=2,xlabel="t",ylabel="s(t)",label="")
```
"""
function cdre(a,b,q,r,s₁,tspan)
    t₀ = tspan[1]
    t₁ = tspan[2]
    f(s,p,t) = -2*a*s - q + b^2/r*s^2           # Corresponds to: -d/dt s(t) = 2as + q - b²/r s²
    prob = ODEProblem(f,s₁,(t₁,t₀))
    sol = solve(prob,Tsit5(),reltol=1e-8,abstol=1e-8)
    return reverse(sol.t),reverse(sol.u)
end

"""
    cdre(A,B,Q,R,S₁,tspan)

Solve a matrix differential Riccati equation (CDRE) ``-d/dt S = SA + A^T S + Q - SBR^{-1}B^TS`` related to the continuous-time LQ-optimal control (regulation).

Consider a fixed time interval (0.0, t₁), on which a continuous-time LTI system described by ``\dot x = A x + B u`` should be regulated such that the cost function ``1/2 x^T(t_1)Sx(t_1) + \int_0^{t_1} (x^T(t)Qx(t) + u^T(t)Ru(t) dt)`` is minimized for any initial state ``x(0)``. The optimal control is given by solving the differential Riccati equation ``-d/dt S = SA + A^T S + Q - SBR^{-1}B^TS``, for which the value is fixed to `s₁` at the final time.

# Example
```julia
n = 2                       # order of the system
m = 1                       # number of inputs
A = rand(n,n)
B = rand(n,m)

q = 1
r = 1
s = 1
Q = diagm(0=>q*ones(n))     # weighting matrices for the quadratic cost function
R = diagm(0=>r*ones(m))
S₁ = diagm(0=>s*ones(n))
t₀ = 0.0
t₁ = 30.0;
tspan = (t₀,t₁)

t,S = cdre(A,B,Q,R,S₁,tspan)

SS = [[z[i,j] for z in S] for i=1:n, j=1:m]     # for plotting purposes

using Plots

plot(t,SS[1,1],linewidth=2,xlabel="t",ylabel="S11",label="")
```
"""
function cdre(A::Array{Float64,2},B,Q,R,S₁,tspan)
    t₀ = tspan[1]
    t₁ = tspan[2]
    f(S,p,t) = -S*A - A'*S -Q + S*B/R*B'*S
    prob = ODEProblem(f,S₁,(t₁,t₀))
    sol = solve(prob,Tsit5(),reltol=1e-8,abstol=1e-8)
    return reverse(sol.t),reverse(sol.u)
end

"""
    t,K,S = clqrtv(A,B,Q,R,S₁,(t₀,t₁))

Time-varying LQ-optimal state feedback gains.

For a continuous-time LTI system parameterized by matrices `A` and `B` find a time-varying state-feedback gains `K` minimizing the standard quadratic cost parameterized by matrices `Q`, `R`, and `S`. Along with the array of state feedback gains it also returns the corresponding time sequence. It also returns the solution to Riccati equation.

# Example
```julia
n = 2                       # order of the system
m = 1                       # number of inputs
A = rand(n,n)
B = rand(n,m)

q = 1
r = 1
s = 1
Q = diagm(0=>q*ones(n))     # weighting matrices for the quadratic cost function
R = diagm(0=>r*ones(m))
S₁ = diagm(0=>s*ones(n))
t₀ = 0.0
t₁ = 30.0;
tspan = (t₀,t₁)

t,K,S = clqrtv(A,B,Q,R,S₁,tspan)
```
"""
function clqrtv(A,B,Q,R,S₁,(t₀,t₁))
    t,S = cdre(A,B,Q,R,S₁,(t₀,t₁))
    K = R/B'*S
    return t,K,S
end

"""
    indirect_cocp_fixed(L,f,x₀,x₁,tspan)

Indirect approach to the numerical solution of a continuous-time optimal control problem (OCP) with fixed initial and final states.

The cost function to be minimized for the continuous-time optimal control problem (OCP) handled by this solver is ``\int_{t_0}^{t_1} L(x,u,t) dt`` for the system modelled by ``\dot x = f(x,u,t)``, with the initial and final values of the state vectors given, that is ``x(t_0)=r_0`` and ``x(t_1)=r_1``.
"""
function indirect_cocp_fixed(L,f,x₀,tspan)
    t0 = tspan[1]                       # Initial time
    t1 = tspan[2]                       # Final time
    n = length(x₀)                      # Order of the system
    ∇L = (x,u)->Zygote.gradient(L,x,u)
    ∇f = (x,u)->Zygote.gradient(f,x,u)
    function statecostateeq!(dw,w,p,t)
        x = w[1:n]
        λ = w[n+1:2n]
        Hᵤ()
        function stationarityeq!(L,f,w)
            fd
        end
        du[1:n] = f(x,u,t)
        du[n+1,2n] = ∇ᵤ
    end
    function bc!(residual, u, p, t)
        residual[1] =
        residual[2] =
    end
    bvp1 = TwoPointBVProblem(statecostateeq!, bc!, u0, tspan)
    sol1 = solve(bvp1, GeneralMIRK4(), dt=0.05)
    return t,x,λ,u
end
