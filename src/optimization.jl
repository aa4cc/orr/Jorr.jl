"""
    α = backtracking_line_search(f,∇f,x₀,d;α₀,γ=0.1,β=0.5)

Solve a scalar optimization problem approximately using the method of backtracking.

Consider a scalar function `f()` of a vector variable `x` restricted to a line given by `x₀+αd`, where `d` is a vector that determines a direction and `α≧0` is the search parameter that determines howe far alond the direction `d` we have to go from `x₀` in order to achieve a sufficient decrease of `f()`.

The method starts with setting `α` to `α₀` (default `α₀`=1) and checks satisfaction of the (Armijo) sufficient descent condition parameterized by `γ` (default `γ`=0.2). If the condition is not satisfied, the length of the step is reduced through `α=βα` (default `β`=0.5).

# Examples

```julia
julia> f(x) = x[1]^2+x[1]*x[2]+x[2]^2
f (generic function with 1 method)

julia> ∇f(x) = [2x[1]+x[2],2x[2]+x[1]]
∇f (generic function with 1 method)

julia> x₀ = [1,2]
2-element Array{Int64,1}:
 1
 2

julia> d = [-1,-1]
2-element Array{Int64,1}:
 -1
 -1

julia> α₀ = 10
10

julia> α = backtracking_line_search(f,∇f,x₀,d,α₀)
2.5
```
"""
function backtracking_line_search(f,∇f₀,x₀,d;α₀=1.0,γ=0.1,β=0.5)
    α = α₀
    f₀ = f(x₀)
    while f₀-f(x₀+α*d) < -γ*α*dot(d,∇f₀)
        α *= β
    end
    return α
end

"""
    x = gradient_descent_minimization(f,x₀;ϵ=1e-8,kmax=2000)

For a scalar function `f()` of a vector argument `x`, find a (local) minimum using the steepest descent (aka gradient) method.

The initial estimate of the solution is `x₀`. The iteration finishes either if the maximum number of iterations `kmax` is reached or if the norm of the gradient gets below `ϵ` (the function is flat enough).

# Examples

```julia
julia> f(x) = (x[1]+2x[2]-7)^2 + (2x[1]+x[2]-5)^2
f (generic function with 1 method)

julia> x₀ = [1.0,1.0]
2-element Array{Float64,1}:
 1.0
 1.0

julia> x = gradient_descent_minimization(f,x₀)
k =    0   ||∇f(x)|| = 2.56e+01   α = 6.25e-02
k =    1   ||∇f(x)|| = 4.03e+00   α = 1.25e-01
k =    2   ||∇f(x)|| = 4.39e+00   α = 6.25e-02
k =    3   ||∇f(x)|| = 1.70e+00   α = 5.00e-01
k =    4   ||∇f(x)|| = 3.98e+00   α = 6.25e-02
k =    5   ||∇f(x)|| = 4.97e-01   α = 6.25e-02
k =    6   ||∇f(x)|| = 6.21e-02   α = 6.25e-02
k =    7   ||∇f(x)|| = 7.77e-03   α = 6.25e-02
k =    8   ||∇f(x)|| = 9.71e-04   α = 6.25e-02
k =    9   ||∇f(x)|| = 1.21e-04   α = 6.25e-02
k =   10   ||∇f(x)|| = 1.52e-05   α = 6.25e-02
k =   11   ||∇f(x)|| = 1.90e-06   α = 6.25e-02
k =   12   ||∇f(x)|| = 2.37e-07   α = 6.25e-02
k =   13   ||∇f(x)|| = 2.96e-08   α = 6.25e-02
2-element Array{Float64,1}:
 0.9999999998544808
 2.999999999854481
```
"""
function gradient_descent_minimization(f,x₀;ϵ=1e-8,kmax=1000)
    k = 0
    xₖ = x₀
    ∇f = x->Zygote.gradient(f,x)
    ∇fₖ = ∇f(xₖ)[1]                                     # Zygote returns a tuple.
    while norm(∇fₖ)>ϵ && k<kmax
        αₖ = backtracking_line_search(f,∇fₖ,xₖ,-∇fₖ)
        xₖ = xₖ-αₖ*∇fₖ
        @printf("k = %4d   ||∇f(x)|| = %4.2e   α = %4.2e\n",k,norm(∇fₖ),αₖ)
        ∇fₖ = ∇f(xₖ)[1]                                 # Zygote returns a tuple.
        k += 1
    end
    return xₖ
end

"""
    x = newton_minimization(f,x₀;ϵ=1e-8,kmax=2000)

For a scalar function `f()` of a vector argument `x`, find a (local) minimum using the Newton's method.

The initial estimate of the solution is `x₀`. The iteration finishes either if the maximum number of iterations `kmax` is reached or if the norm of the gradient gets below `ϵ` (the function is flat enough).

# Examples

```julia
julia> f(x) = (x[1]+2x[2]-7)^2 + (2x[1]+x[2]-5)^2
f (generic function with 1 method)

julia> x₀ = [1.0,1.0]
2-element Array{Float64,1}:
 1.0
 1.0

julia> x = newton_minimization(f,x₀)
k =    0   ||∇f(x)|| = 2.56e+01   α = 6.25e-02
k =    1   ||∇f(x)|| = 4.03e+00   α = 1.25e-01
k =    2   ||∇f(x)|| = 4.39e+00   α = 6.25e-02
k =    3   ||∇f(x)|| = 1.70e+00   α = 5.00e-01
k =    4   ||∇f(x)|| = 3.98e+00   α = 6.25e-02
k =    5   ||∇f(x)|| = 4.97e-01   α = 6.25e-02
k =    6   ||∇f(x)|| = 6.21e-02   α = 6.25e-02
k =    7   ||∇f(x)|| = 7.77e-03   α = 6.25e-02
k =    8   ||∇f(x)|| = 9.71e-04   α = 6.25e-02
k =    9   ||∇f(x)|| = 1.21e-04   α = 6.25e-02
k =   10   ||∇f(x)|| = 1.52e-05   α = 6.25e-02
k =   11   ||∇f(x)|| = 1.90e-06   α = 6.25e-02
k =   12   ||∇f(x)|| = 2.37e-07   α = 6.25e-02
k =   13   ||∇f(x)|| = 2.96e-08   α = 6.25e-02
2-element Array{Float64,1}:
 0.9999999998544808
 2.999999999854481
```
"""
function newton_minimization(f,x₀;ϵ=1e-8,kmax=1000)
    k = 0
    xₖ = x₀
    ∇f = x->Zygote.gradient(f,x)
    ∇²f = x->Zygote.hessian(f,x)
    ∇fₖ = ∇f(xₖ)[1]                                     # Zygote returns a tuple.
    ∇²fₖ = ∇²f(xₖ)
    while norm(∇fₖ)>ϵ && k<kmax
        xₖ = xₖ-∇²fₖ\∇fₖ
        @printf("k = %4d   ||∇f(x)|| = %4.2e\n",k,norm(∇fₖ))
        ∇fₖ = ∇f(xₖ)[1]                                 # Zygote returns a tuple.
        ∇²fₖ = ∇²f(xₖ)
        k += 1
    end
    return xₖ
end

"""
    x = quasi_newton_minimization(f,x₀;ϵ=1e-8,kmax=2000,update=:BFGS)

For a scalar function `f()` of a vector argument `x`, find a (local) minimum using of the method from the family of Quasi-Newton methods.

The initial estimate of the solution is `x₀`. The iteration finishes either if the maximum number of iterations `kmax` is reached or if the norm of the gradient gets below `ϵ` (the function is flat enough).

# Examples

```julia
julia> f(x) = (x[1]+2x[2]-7)^2 + (2x[1]+x[2]-5)^2
f (generic function with 1 method)

julia> x₀ = [1.0,1.0]
2-element Array{Float64,1}:
 1.0
 1.0

julia> x = quasi_newton_minimization(f,x₀)
2-element Array{Float64,1}:
 1.0000000000000002
 3.0

```
"""
function quasi_newton_minimization(f,x₀;ϵ=1e-8,kmax=1000,update=:BFGS)
    k = 0
    xₖ = x₀
    ∇f = x->Zygote.gradient(f,x)
    ∇fₖ = ∇f(xₖ)[1]
    n = length(x₀)
    Hₖ = Matrix{Int}(I,n,n)
    while norm(∇fₖ)>ϵ && k<kmax
        x₊ = xₖ-Hₖ*∇fₖ
        ∇f₊ = ∇f(x₊)[1]
        sₖ = x₊-xₖ
        yₖ = ∇f₊-∇fₖ
        if isequal(update,:BFGS)
            sy = sₖ⋅yₖ
            Hₖ += (1+dot(yₖ,Hₖ*yₖ)/sy)*(sₖ*sₖ')/sy - (sₖ*yₖ'*Hₖ+Hₖ*yₖ*sₖ')/sy
        else
            throw(ArgumentError("No such update scheme implemented (yet)."))
        end
        k += 1
        xₖ = x₊
        ∇fₖ = ∇f₊
    end
    return xₖ
end
