"""
    Js,us = design_dp_lookup_tables_1(f,ϕ,L,x,u,N)

Design dynamic programming lookup tables for an optimal control problem for a scalar system.

Consider a scalar (=first-order) discrete-time system described by `x_{k+1} = f(x_k,u_k)`. Consider a discrete time interval `[i,N]`. Consider also a cost function ``J_i(x_i,u_i,\ldots,u_{N-1}) = \phi(x_N) + \sum_{k=i}^{N-1}L(x_k,u_k)``. Produce two arrays (or tables), both of size `nᵪ×N`,one whose entries are the optimal costs-to-go ``J_k^✳(x_k)``, the other with the optimal control ``u_k(x_k)``. The `x` and `u` input variables are gridded state and control input variables.
"""
function design_dp_lookup_tables_1(f,ϕ,L,x,u,N)
    n = length(x)                           # number of grid points for x
    m = length(u)                           # number of grid points for u
    Js = Matrix{Number}(undef,n,N+1)        # allocation of the table for J*
    Us = Matrix{Number}(undef,n,N)          # allocation of the table for u*
    for i=1:n                               # evaluating the cost at the final time
        Js[i,N+1] = ϕ(x[i])                 # (N+1)-th column corresponds to J at k=N
    end
    for k=N:-1:1                            # iterating over time, but k=N is for time N-1
        Jsintp = LinearInterpolation(x,Js[:,k+1])   # preparing for interpolations
        for i=1:n                           # iterating over grid points in the x space
            Ju = Vector{Number}(undef,m)    # unstarred J for the individual discretized u
            for j=1:m                       # iterating over grid points in the u space
                xnext = f(x[i],u[j])        # prediction of the next state
                if xnext>x[1] && xnext<x[end]     # detects if inside the range
                    Ju[j] = L(x[i],u[j]) + Jsintp(xnext)
                else
                    Ju[j] = Inf
                end
            end
            Js[i,k] = minimum(Ju)
            Us[i,k] = isfinite(Js[i,k]) ? u[argmin(Ju)] : NaN
        end
    end
    return Js, Us
end

"""
    dp_sim(a,b,U,xgrid,x0,N)

Simulate a response of a first-order system ``x_{k+1} = f(x_k,u_k)`` with a dynamic programming-based feedback controller in the form of a lookup table `U`. The initial time is `i` and the final time is `N`. The initial state is `x0`. The bounds on the state variable `x` are entered simultaneously with its grid as the `xgrid` vector variable.
"""
function dp_sim(f,U,xgrid,x0,N)
    x = Vector{Float64}(undef, N+1)                 # prepares an array for state response
    u = Vector{Float64}(undef, N)                   # prepares an array for controls
    x[1] = x0
    for k=1:N                                       # remember the [0,N-1] -> [1,N] shift
        uintp = LinearInterpolation(xgrid,U[:,k])
        u[k] = uintp(x[k])                          # interpolation in the look-up table
        x[k+1] = f(x[k],u[k])
    end
    return x, u
end

"""
    demo_dp_1()

Demonstration of a dynamic programming-based feedback control design and simulation.

First-order discrete-time LTI system with bounds on the control input and state variables and a finite time horizon. LQ cost function used.
"""
function demo_dp_1()
    a = -5/3
    b = 2
    sN = 5
    q = 2
    r = 1
    f(x,u) = a*x + b*u
    ϕ(xN) = 1/2*sN*xN^2
    L(x,u) = 1/2*(q*x^2 + r*u^2)
    xgrid = -3:0.5:3
    ugrid = -0.6:0.1:0.6
    N = 10
    Js,Us = design_dp_lookup_tables_1(f,ϕ,L,xgrid,ugrid,N)
    plot(x,Js)
    x0 = 2.5
    xsim, usim = dp_sim(f,Us,xgrid,x0,N)
    p1 = plot(0:N,xsim)
    xlabel!("k")
    ylabel!("State")
    p2 = plot(0:N-1,usim,xlims=xlims(p1))
    xlabel!("k")
    ylabel!("Control")
    plot(p1,p2,layout=(2,1))
end
